;IMPORTANT NOTICE : PUT THIS LINE IF YOU WANT TO USE IMAGESEARCH
;IMPORTANT IMPORTANT IMPORTANT
;IMPORTANT IMPORTANT IMPORTANT
#AutoIt3Wrapper_UseX64=n
;IMPORTANT IMPORTANT IMPORTANT
;IMPORTANT IMPORTANT IMPORTANT
;IMPORTANT IMPORTANT IMPORTANT


#include <Utils\ImageSearch.au3>

;
; Demo on the functions of ImageSearch
; Assumes that you have a Recycle Bin icon at the top left of your screen
; Assumes that you have IE6 or 7 icon visible
; Please make the icon visible or we won't be able to find it
;
While 1
	HotKeySet("{#}","Exasd")
	Sleep(100)
WEnd

Func Exasd()
	ConsoleWrite("Testing" & @LF)
	$x1=0
	$y1=0
	$file = "Images\ResourceIcon"
	$result = _ImageSearchArea($file & ".bmp",1,0,0,5000,2000,$x1,$y1,100)
	if $result==1 Then
		MouseMove($x1,$y1,3)
	EndIf
EndFunc