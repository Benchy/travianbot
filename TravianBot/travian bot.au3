#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_UseX64=n
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****

#include "include.au3"
 #include <GuiButton.au3>


#Region Generated Code

	#include <ButtonConstants.au3>
	#include <EditConstants.au3>
	#include <GUIConstantsEx.au3>
	#include <ListViewConstants.au3>
	#include <StaticConstants.au3>
	#include <TabConstants.au3>
	#include <WindowsConstants.au3>
	Opt("GUIOnEventMode", 1)
	#Region ### START Koda GUI section ### Form=c:\users\benoit\desktop\travianbot\travianbot.kxf
	$Form1 = GUICreate("Travian Bot", 616, 590, 186, 127)
	GUISetOnEvent($GUI_EVENT_CLOSE, "Form1Close")
	GUISetOnEvent($GUI_EVENT_MINIMIZE, "Form1Minimize")
	GUISetOnEvent($GUI_EVENT_MAXIMIZE, "Form1Maximize")
	GUISetOnEvent($GUI_EVENT_RESTORE, "Form1Restore")
	$Tab1 = GUICtrlCreateTab(16, 16, 593, 321)
	GUICtrlSetResizing(-1, $GUI_DOCKWIDTH+$GUI_DOCKHEIGHT)
	$TabLand = GUICtrlCreateTabItem("Land")
	$AutoLevelResourceButton = GUICtrlCreateButton("Auto level resources", 32, 64, 115, 25, $WS_GROUP)
	GUICtrlSetOnEvent(-1, "AutoLevelResourceButtonClick")
	$AutoLevelResourceButton = GUICtrlCreateButton("Auto level resources No Crop", 32, 94, 150, 25, $WS_GROUP)
	GUICtrlSetOnEvent(-1, "NoCropButtonClick")
	$AutoLevelResourceButton = GUICtrlCreateButton("Wood", 32, 124, 115, 25, $WS_GROUP)
	GUICtrlSetOnEvent(-1, "WoodButtonClick")
	$AutoLevelResourceButton = GUICtrlCreateButton("Clay", 32, 154, 115, 25, $WS_GROUP)
	GUICtrlSetOnEvent(-1, "ClayButtonClick")
	$AutoLevelResourceButton = GUICtrlCreateButton("Iron", 32, 184, 115, 25, $WS_GROUP)
	GUICtrlSetOnEvent(-1, "IronButtonClick")
	$AutoLevelResourceButton = GUICtrlCreateButton("Crop", 32, 214, 115, 25, $WS_GROUP)
	GUICtrlSetOnEvent(-1, "CropButtonClick")

	$TabActions = GUICtrlCreateTabItem("Actions")
	GUICtrlSetState(-1,$GUI_SHOW)
	$Adventure = GUICtrlCreateButton("Adventure", 32, 64, 115, 25, $WS_GROUP)
	GUICtrlSetOnEvent(-1, "AdventureClick")

	$TabVillage = GUICtrlCreateTabItem("Village")
	GUICtrlSetState(-1,$GUI_SHOW)
	$BarrackButton = GUICtrlCreateButton("Barrack", 504, 112, 75, 25, $WS_GROUP)
	GUICtrlSetOnEvent(-1, "BarrackButtonClick")
	$WareHouseButton = GUICtrlCreateButton("WareHouse", 504, 144, 75, 25, $WS_GROUP)
	GUICtrlSetOnEvent(-1, "WareHouseButtonClick")
	$GranaryButton = GUICtrlCreateButton("Granary", 504, 176, 75, 25, $WS_GROUP)
	GUICtrlSetOnEvent(-1, "GranaryButtonClick")
	$TradeOfficeButton = GUICtrlCreateButton("TradeOffice", 504, 208, 75, 25, $WS_GROUP)
	GUICtrlSetOnEvent(-1, "TradeOfficeButtonClick")
	$WindMillButton = GUICtrlCreateButton("WindMill", 416, 264, 75, 25, $WS_GROUP)
	GUICtrlSetOnEvent(-1, "WindMillButtonClick")
	$SawMillButton = GUICtrlCreateButton("SawMill", 336, 264, 75, 25, $WS_GROUP)
	GUICtrlSetOnEvent(-1, "SawMillButtonClick")
	$BrickYardButton = GUICtrlCreateButton("BrickYard", 256, 264, 75, 25, $WS_GROUP)
	GUICtrlSetOnEvent(-1, "BrickYardButtonClick")
	$IronFoundryButton = GUICtrlCreateButton("IronFoundry", 168, 264, 75, 25, $WS_GROUP)
	GUICtrlSetOnEvent(-1, "IronFoundryButtonClick")
	$MainBuildingButton = GUICtrlCreateButton("MainBuilding", 272, 120, 75, 25, $WS_GROUP)
	GUICtrlSetOnEvent(-1, "MainBuildingButtonClick")
	$RallyPointButton = GUICtrlCreateButton("RallyPoint", 328, 160, 75, 25, $WS_GROUP)
	GUICtrlSetOnEvent(-1, "RallyPointButtonClick")
	$TounrementButton = GUICtrlCreateButton("Tounrement", 312, 200, 75, 25, $WS_GROUP)
	GUICtrlSetOnEvent(-1, "TounrementButtonClick")
	$PalaceButton = GUICtrlCreateButton("Palace", 232, 200, 75, 25, $WS_GROUP)
	GUICtrlSetOnEvent(-1, "PalaceButtonClick")
	$MarketPlaceButton = GUICtrlCreateButton("MarketPlace", 216, 160, 75, 25, $WS_GROUP)
	GUICtrlSetOnEvent(-1, "MarketPlaceButtonClick")
	$BakeryButton = GUICtrlCreateButton("Bakery", 32, 224, 75, 25, $WS_GROUP)
	GUICtrlSetOnEvent(-1, "BakeryButtonClick")
	$AcademyButton = GUICtrlCreateButton("Academy", 32, 192, 75, 25, $WS_GROUP)
	GUICtrlSetOnEvent(-1, "AcademyButtonClick")
	$EmbassyButton = GUICtrlCreateButton("Embassy", 32, 160, 75, 25, $WS_GROUP)
	GUICtrlSetOnEvent(-1, "EmbassyButtonClick")
	$WorkShopButton = GUICtrlCreateButton("WorkShop", 32, 128, 75, 25, $WS_GROUP)
	GUICtrlSetOnEvent(-1, "WorkShopButtonClick")
	$StoneMassonButton = GUICtrlCreateButton("StoneMasson", 144, 64, 75, 25, $WS_GROUP)
	GUICtrlSetOnEvent(-1, "StoneMassonButtonClick")
	$TownHallButton = GUICtrlCreateButton("TownHall", 232, 64, 75, 25, $WS_GROUP)
	GUICtrlSetOnEvent(-1, "TownHallButtonClick")
	$StableButton = GUICtrlCreateButton("Stable", 320, 64, 75, 25, $WS_GROUP)
	GUICtrlSetOnEvent(-1, "StableButtonClick")
	$SmithyButton = GUICtrlCreateButton("Smithy", 404, 64, 75, 25, $WS_GROUP)
	GUICtrlSetOnEvent(-1, "SmithyButtonClick")
	$PalisadeButton = GUICtrlCreateButton("Palisade", 80, 304, 467, 25, $WS_GROUP)
	GUICtrlSetOnEvent(-1, "PalisadeButtonClick")



	$test = GUICtrlCreateButton("", 0, 0, 1, 1, $WS_GROUP)

	GUICtrlCreateTabItem("")
	GUICtrlSetOnEvent(-1, "Tab1Change")
	$BtnStart = GUICtrlCreateButton("Start", 24, 352, 75, 25, $WS_GROUP)
	GUICtrlSetOnEvent(-1, "BtnStartClick")
	$BtnClear = GUICtrlCreateButton("Clear All", 24, 382, 75, 25, $WS_GROUP)
	GUICtrlSetOnEvent(-1, "ClearAll")
	$loopCheckBox = GUICtrlCreateCheckbox("Loop",24, 412)
	Global $VillageCombo = GUICtrlCreateCombo("Sorel", 24, 442, 100, 25)
	GUICtrlSetData(-1, $villages)
	GUISetState(@SW_SHOW)
	GUICtrlSetOnEvent(-1, "VillageChanged")
	$ListAction = GUICtrlCreateListView("", 136, 368, 250, 150)
	GUICtrlSetOnEvent(-1, "ListActionClick")
	$Label1 = GUICtrlCreateLabel("Action Queue", 136, 344, 69, 17)
	GUICtrlSetOnEvent(-1, "Label1Click")
	$LogTxtBox = GUICtrlCreateEdit("", 400, 368, 193, 145)
	GUICtrlSetData(-1, "LogTxtBox")
	GUICtrlSetOnEvent(-1, "LogTxtBoxChange")
	$Label2 = GUICtrlCreateLabel("Logs", 408, 344, 27, 17)
	GUICtrlSetOnEvent(-1, "Label2Click")
	GUISetState(@SW_SHOW)


	#EndRegion ### END Koda GUI section ###







#endregion ;End Generated Code


	Init()
	While 1
		if($Botting) Then

			For $i = 0 To $SleepTime.Count() - 1 Step 1
				if $SleepTime.Item($i) <= 0 Then
					Update($i)
				EndIf
			Next

			For $i = 0 To $SleepTime.Count() - 1 Step 1
				$SleepTime.Item($i) = $SleepTime.Item($i) - 1
			Next
			Sleep(900)
		EndIf
		Sleep(100)
	WEnd

	Func AutoLevelResourceButtonClick()
		AddAction($ACTION_AUTORESOURCE)
	EndFunc
	Func BakeryButtonClick()
		AddAction($ACTION_BAKERY)
	EndFunc
	Func BarrackButtonClick()
		AddAction($ACTION_BARRACK)
	EndFunc
	Func BrickYardButtonClick()
		AddAction($ACTION_BRICKYARD)
	EndFunc
	Func AcademyButtonClick()
		AddAction($ACTION_ACADEMY)
	EndFunc
	Func SmithyButtonClick()
		AddAction($ACTION_SMITHY)
	EndFunc
	Func StableButtonClick()
		AddAction($ACTION_STABLE)
	EndFunc
	Func TownHallButtonClick()
		AddAction($ACTION_TOWNHALL)
	EndFunc
	Func TradeOfficeButtonClick()
		AddAction($ACTION_TRADEOFFICE)
	EndFunc
	Func WorkShopButtonClick()
		AddAction($ACTION_WORKSHOP)
	EndFunc
	Func EmbassyButtonClick()
		AddAction($ACTION_EMBASSY)
	EndFunc
	Func GranaryButtonClick()
		AddAction($ACTION_GRANARY)
	EndFunc
	Func IronFoundryButtonClick()
		AddAction($ACTION_IRONFOUNDRY)
	EndFunc
	Func MainBuildingButtonClick()
		AddAction($ACTION_MAINBUILDING)
	EndFunc
	Func MarketPlaceButtonClick()
		AddAction($ACTION_MARKETPLACE)
	EndFunc
	Func PalaceButtonClick()
		AddAction($ACTION_PALACE)
	EndFunc
	Func PalisadeButtonClick()
		AddAction($ACTION_PALISADE)
	EndFunc
	Func RallyPointButtonClick()
		AddAction($ACTION_RALLYPOINT)
	EndFunc
	Func SawMillButtonClick()
		AddAction($ACTION_SAWMILL)
	EndFunc
	Func StoneMassonButtonClick()
		AddAction($ACTION_STONEMASSON)
	EndFunc
	Func TounrementButtonClick()
		AddAction($ACTION_TOURNEMANT)
	EndFunc
	Func WareHouseButtonClick()
		AddAction($ACTION_WAREHOUSE)
	EndFunc
	Func WindMillButtonClick()
		AddAction($ACTION_MILL)
	EndFunc
	Func NoCropButtonClick()
		AddAction($ACTION_AUTORESOURCENOCROP)
	EndFunc
	Func WoodButtonClick()
		AddAction($ACTION_WOOD)
	EndFunc
	Func ClayButtonClick()
		AddAction($ACTION_CLAY)
	EndFunc
	Func IronButtonClick()
		AddAction($ACTION_IRON)
	EndFunc
	Func CropButtonClick()
		AddAction($ACTION_CROP)
	EndFunc

	Func BtnStartClick()
		$Botting = Not $Botting

		if $Botting Then
			_GUICtrlButton_SetText($BtnStart,"Stop")
		Else
			_GUICtrlButton_SetText($BtnStart,"Start")
		EndIf

	EndFunc

	Func Form1Close()
		Exit(0)
	EndFunc
	Func Form1Maximize()

	EndFunc
	Func Form1Minimize()

	EndFunc
	Func Form1Restore()

	EndFunc
	Func ListActionClick()

	EndFunc
	Func LogTxtBoxChange()

	EndFunc
	Func Tab1Change()

	EndFunc
	Func Label1Click()

	EndFunc
	Func Label2Click()

	EndFunc
	Func AdventureClick()
		AddAction($ACTION_ADVENTURE)
	EndFunc

	Func VillageChanged()
		Local $index = _GUICtrlComboBox_GetCurSel($VillageCombo)

		$CurrentVillageIndex = $index
		_Log("village index : " & $CurrentVillageIndex)
		UpdateVisual()
	EndFunc

	Func ClearAll()
		$ActionQueue.Item($CurrentVillageIndex).Clear()
		UpdateVisual()
	EndFunc

	Func Update($villageIndex)
		;Change village
		ChangeVillage($villageIndex)

		$worker = GetWorkerCount()

		if $worker < 1 Then
			FormatWindow()
			PerformAction()
		Else
			$SleepTime.Item($CurrentVillageIndex) = 60
		endif
	EndFunc

