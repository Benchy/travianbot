Func GetWorkerCount()
	Local $workers = TesseractRead($POSITION_WORKERCOUNT,1,10)

	_Log($workers & " workers unavailable")
	Return $workers
EndFunc

Func GetWoodCount()
	Local $woodCountFull = TesseractRead($POSITION_WOODCOUNT,0)
	Local $woodCount = StringMid($woodCountFull,1, StringInStr($woodCountFull,"/") -1)
	Return Number($woodCount)
EndFunc

Func GetClayCount()
	Local $ClayCountFull = TesseractRead($POSITION_CLAYCOUNT,0)
	Local $ClayCount = StringMid($ClayCountFull,1, StringInStr($ClayCountFull,"/") -1)
	Return Number($ClayCount)
EndFunc

Func GetIronCount()
	Local $ironCountFull = TesseractRead($POSITION_IRONCOUNT,0)
	Local $ironCount = StringMid($ironCountFull,1, StringInStr($ironCountFull,"/") -1)
	Return Number($ironCount)
EndFunc

Func GetCropCount()
	Local $cropCountFull = TesseractRead($POSITION_CROPCOUNT,0)
	Local $cropCount = StringMid($cropCountFull,1, StringInStr($cropCountFull,"/") -1)
	Return Number($cropCount)
EndFunc

Func PrintResources()
	_Log("Wood : " & GetWoodCount())
	_Log("Clay : " & GetClayCount())
	_Log("Iron : " & GetIronCount())
	_Log("Crop : " & GetCropCount())
EndFunc

Func Upgrade()
	Local $upgradeX = 0
	Local $upgradeY = 0
	if _ImageSearchAreaHwnd($IMAGE_UPGRADE,1, _
	0, _
	0, _
	1000, _
	1000, _
	$upgradeX, $upgradeY, 10,$Hwnd) == 1 Then
		FindRemainingTime()
		ClickGlobal($upgradeX,$upgradeY)
		Return 1
	Else
		_Log("Error : Didnt find upgrade button")
		Return 0
	EndIf
EndFunc

Func FindRemainingTime()
	Local $x = 0
	Local $y= 0
	if _ImageSearchAreaHwnd($IMAGE_TIME,1, _
		0, _
		0, _
		1000, _
		1000, _
		$x, $y, 10,$Hwnd) == 1 Then

			$position = WinGetPos($Hwnd)
			$x = $x - $position[0]
			$y = $y - $position[1]

			Local $time = TesseractReadPoint($x+11,$y-8,$x+75,$y+8)
			_Log($time)
			Local $hours = StringMid($time,1,2)
			Local $min = StringMid($time,4,2)
			Local $sec = StringMid($time,7,2)

			Local $NewSleepTime = 0
			$NewSleepTime  = $NewSleepTime  + $sec +10
			$NewSleepTime = $NewSleepTime + ($min * 60)
			$NewSleepTime = $NewSleepTime  + ($hours * 60 * 60)

			$SleepTime.Item($CurrentVillageIndex) = $NewSleepTime
	EndIf
EndFunc