Global $CurrentVillageIndex = 0
Global $villages = "Emilie" ;ajouter les autre separer avec un |

#region Attribute
;------------------
;RESOURCES 3D ARRAYS
;------------------
Global $woodFields = ObjCreate('System.Collections.ArrayList')
Global $ironFields = ObjCreate('System.Collections.ArrayList')
Global $clayFields = ObjCreate('System.Collections.ArrayList')
Global $cropFields = ObjCreate('System.Collections.ArrayList')


;------------------
;BUILDINGS 2D ARRAYS
;------------------
Global $MainBuilding = ObjCreate('System.Collections.ArrayList')
Global $RallyPoint = ObjCreate('System.Collections.ArrayList')
Global $Tournemant = ObjCreate('System.Collections.ArrayList')
Global $Palace = ObjCreate('System.Collections.ArrayList')
Global $MarketPlace = ObjCreate('System.Collections.ArrayList')

Global $Barrack = ObjCreate('System.Collections.ArrayList')
Global $WareHouse = ObjCreate('System.Collections.ArrayList')
Global $Granary = ObjCreate('System.Collections.ArrayList')
Global $TradeOffice = ObjCreate('System.Collections.ArrayList')

Global $Mill = ObjCreate('System.Collections.ArrayList')
Global $SawMill = ObjCreate('System.Collections.ArrayList')
Global $BrickYard = ObjCreate('System.Collections.ArrayList')
Global $IronFoundry = ObjCreate('System.Collections.ArrayList')

Global $Bakery = ObjCreate('System.Collections.ArrayList')
Global $Academy = ObjCreate('System.Collections.ArrayList')
Global $Embassy = ObjCreate('System.Collections.ArrayList')
Global $Workshop = ObjCreate('System.Collections.ArrayList')

Global $StoneMasson = ObjCreate('System.Collections.ArrayList')
Global $TownHall = ObjCreate('System.Collections.ArrayList')
Global $Stable = ObjCreate('System.Collections.ArrayList')
Global $Smithy = ObjCreate('System.Collections.ArrayList')

Global $Palisade = ObjCreate('System.Collections.ArrayList')

;------------------
;ACTIONS
;------------------
Global $ActionBarrack = ObjCreate('System.Collections.ArrayList')
Global $ActionRallyPoint = ObjCreate('System.Collections.ArrayList')



#endregion
Func InitVillages()

	;Village 1
	;--W------W
	;---------
	;-----W----
	;-----W----
	AddCoord_3D($woodFields,523,418)
	AddCoord_3D($woodFields,833,380)
	AddCoord_3D($woodFields,690,679)
	AddCoord_3D($woodFields,701,777)

	;----C--C----
	;-----------
	;--C-------C-
	AddCoord_3D($clayFields,650,462)
	AddCoord_3D($clayFields,744,447)
	AddCoord_3D($clayFields,575,716)
	AddCoord_3D($clayFields,822,715)

	;--I----------
	;----------I---
	;--------I---I-
	AddCoord_3D($ironFields,414,481)
	AddCoord_3D($ironFields,945,440)
	AddCoord_3D($ironFields,881,505)
	AddCoord_3D($ironFields,1060,516)

	;----------C----------
	;--C--C--------------
	;--C--C-------------C
	AddCoord_3D($cropFields,660,349)
	AddCoord_3D($cropFields,341,596)
	AddCoord_3D($cropFields,454,583)
	AddCoord_3D($cropFields,395,675)
	AddCoord_3D($cropFields,518,624)
	AddCoord_3D($cropFields,937,613)

	;Buildings
	AddCoord_2D($MainBuilding,684,525)
	AddCoord_2D($RallyPoint,802,670)
	AddCoord_2D($Tournemant,0,0);CHANGE IT AT BOTH PLACES
	AddCoord_2D($Palace,459,654)
	AddCoord_2D($MarketPlace,527,565)

	AddCoord_2D($Barrack,924,528)
	AddCoord_2D($WareHouse,1027,576)
	AddCoord_2D($Granary,1040,705)
	AddCoord_2D($TradeOffice,935,766)

	AddCoord_2D($Mill,848,854)
	AddCoord_2D($SawMill,0,0) ;CHANGE IT AT BOTH PLACES
	AddCoord_2D($BrickYard,0,0);CHANGE IT AT BOTH PLACES
	AddCoord_2D($IronFoundry,0,0);CHANGE IT AT BOTH PLACES

	AddCoord_2D($Bakery,0,0);CHANGE IT AT BOTH PLACES
	AddCoord_2D($Academy,257,701)
	AddCoord_2D($Embassy,266,587)
	AddCoord_2D($Workshop,0,0);CHANGE IT AT BOTH PLACES


	AddCoord_2D($StoneMasson,478,455)
	AddCoord_2D($TownHall,595,400)
	AddCoord_2D($Stable,734,422)
	AddCoord_2D($Smithy,865,436)

	AddCoord_2D($Palisade,585,900)


	$CurrentVillageIndex = 1
	;Village 2
	;--------W
	;---------
	;-----W----
	;-----W----
	AddCoord_3D($woodFields,833,380)
	AddCoord_3D($woodFields,690,679)
	AddCoord_3D($woodFields,701,777)
	;------C----
	;-----------
	;--C-------C-
	AddCoord_3D($clayFields,744,447)
	AddCoord_3D($clayFields,575,716)
	AddCoord_3D($clayFields,822,715)
	;------------
	;----------I---
	;--------I---I-
	AddCoord_3D($ironFields,945,440)
	AddCoord_3D($ironFields,881,505)
	AddCoord_3D($ironFields,1060,516)
	;----C------
	;---C--C------
	;--C--------
	;C--C--------
	;C--C-------C
	AddCoord_3D($cropFields,660,349)
	AddCoord_3D($cropFields,523,418)
	AddCoord_3D($cropFields,650,462)
	AddCoord_3D($cropFields,414,481)
	AddCoord_3D($cropFields,341,596)
	AddCoord_3D($cropFields,454,583)
	AddCoord_3D($cropFields,395,675)
	AddCoord_3D($cropFields,518,624)
	AddCoord_3D($cropFields,937,613)

	;Buildings
	AddCoord_2D($MainBuilding,684,525)
	AddCoord_2D($RallyPoint,802,670)
	AddCoord_2D($Tournemant,0,0)
	AddCoord_2D($Palace,459,654)
	AddCoord_2D($MarketPlace,527,565)

	AddCoord_2D($Barrack,924,528)
	AddCoord_2D($WareHouse,1027,576)
	AddCoord_2D($Granary,1040,705)
	AddCoord_2D($TradeOffice,935,766)

	AddCoord_2D($Mill,848,854)
	AddCoord_2D($SawMill,0,0)
	AddCoord_2D($BrickYard,0,0)
	AddCoord_2D($IronFoundry,0,0)

	AddCoord_2D($Bakery,0,0)
	AddCoord_2D($Academy,257,701)
	AddCoord_2D($Embassy,266,587)
	AddCoord_2D($Workshop,0,0)


	AddCoord_2D($StoneMasson,478,455)
	AddCoord_2D($TownHall,595,400)
	AddCoord_2D($Stable,734,422)
	AddCoord_2D($Smithy,865,436)

	AddCoord_2D($Palisade,585,900)





	;reset to initial value
	$CurrentVillageIndex = 0

EndFunc