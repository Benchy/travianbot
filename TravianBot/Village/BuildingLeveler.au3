Func LevelBuilding(ByRef $a2dBuilding,$buildingname)

	_Log("=======================")
	_Log("Leveling " & $buildingname)

	ClickVillageView()
	Sleep(2000)

	Local $x = $a2dBuilding.Item($CurrentVillageIndex).Item(0)
	Local $y = $a2dBuilding.Item($CurrentVillageIndex).Item(1)
	Local $tempx = 0
	Local $tempy = 0
	Local $buildingCropingSize = 20
	_Log("X: " & $x & " Y: " & $y)

	$pos = WinGetPos($Hwnd)
	;MouseMove($x + $pos[0],$y + $pos[1])

	;Check if already leveling up
	if _ImageSearchAreaHwnd($IMAGE_RESOURCEICON,1, _
	$x - $buildingCropingSize, _
	$y - $buildingCropingSize, _
	$x + $buildingCropingSize, _
	$y + $buildingCropingSize, _
	$tempx, $tempy, 20,$Hwnd) == 0 Then
		_Log("Cant level up " & $buildingname )

		Return 0
	EndIf


	Click($x,$y)
	Sleep(2000)

	Return Upgrade()
EndFunc


Func ClickVillageView()
	Click($POSITION_VILLAGEVIEW[0],$POSITION_VILLAGEVIEW[1])
	Sleep(2000)
EndFunc