#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_UseX64=n
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****
;IMPORTANT NOTICE : PUT THIS LINE IF YOU WANT TO USE IMAGESEARCH

Func AutoLevelResources()
	_Log("=======================")
	_Log("Auto level resources")

	Local $resCount[4] = [GetWoodCount(), GetClayCount(),GetIronCount(),GetCropCount()]
	Local $minIndex = _ArrayMinIndex($resCount)

	;find best resource to level up
	Local $NextTargetPosition[2]
	if $minIndex == 0 Then
		_Log("Leveling wood")
		$NextTargetPosition = FindLowestResource($woodFields)
	ElseIf $minIndex == 1 Then
		_Log("Leveling clay")
		$NextTargetPosition = FindLowestResource($clayFields)
	ElseIf $minIndex == 2 Then
		_Log("Leveling iron")
		$NextTargetPosition = FindLowestResource($ironFields)
	ElseIf $minIndex == 3 Then
		_Log("Leveling crop")
		$NextTargetPosition = FindLowestResource($cropFields)
	EndIf

	Return ResourceUpgrade($NextTargetPosition)

EndFunc

Func AutoLevelResourcesNoCrop()
	_Log("=======================")
	_Log("Auto level resources No Crop")

	Local $resCount[3] = [GetWoodCount(), GetClayCount(),GetIronCount()]
	Local $minIndex = _ArrayMinIndex($resCount)

	;find best resource to level up
	Local $NextTargetPosition[2]
	if $minIndex == 0 Then
		_Log("Leveling wood")
		$NextTargetPosition = FindLowestResource($woodFields)
	ElseIf $minIndex == 1 Then
		_Log("Leveling clay")
		$NextTargetPosition = FindLowestResource($clayFields)
	ElseIf $minIndex == 2 Then
		_Log("Leveling iron")
		$NextTargetPosition = FindLowestResource($ironFields)
	EndIf


	Return ResourceUpgrade($NextTargetPosition)


EndFunc



Func LevelResources($resource)
	_Log("=======================")
	;find best resource to level up
	Local $NextTargetPosition[2]
	if $resource == $ACTION_WOOD Then
		_Log("Leveling wood")
		$NextTargetPosition = FindLowestResource($woodFields)
	ElseIf $resource == $ACTION_CLAY Then
		_Log("Leveling clay")
		$NextTargetPosition = FindLowestResource($clayFields)
	ElseIf $resource == $ACTION_IRON Then
		_Log("Leveling iron")
		$NextTargetPosition = FindLowestResource($ironFields)
	ElseIf $resource == $ACTION_CROP Then
		_Log("Leveling crop")
		$NextTargetPosition = FindLowestResource($cropFields)
	EndIf


	Return ResourceUpgrade($NextTargetPosition)

EndFunc

Func ResourceUpgrade($NextTargetPosition)
	if $NextTargetPosition[0] <> 0 and $NextTargetPosition[1] <> 0  Then
		Click($NextTargetPosition[0],$NextTargetPosition[1])

		Sleep(1000)

		;Click on Upgrade
		Return Upgrade()
	Else
		Return 0
	EndIf
EndFunc

Func FindLowestResource(ByRef $resource)
	ClickResourceView()

	Local $position[2] = [0,0]
	Local $lowestLevel = 200

	For $i = 0 To $resource.Item($CurrentVillageIndex).Count-1 Step 1

		Local $x = $resource.Item($CurrentVillageIndex).Item($i).Item(0)
		Local $y = $resource.Item($CurrentVillageIndex).Item($i).Item(1)

		Local $tempx = 0
		Local $tempy = 0
		Local $resourceCropingsize = 25

		;Check if already leveling up
		if _ImageSearchAreaHwnd($IMAGE_RESOURCEICON,1, _
		$x - $resourceCropingsize, _
		$y - $resourceCropingsize, _
		$x + $resourceCropingsize, _
		$y + $resourceCropingsize, _
		$tempx, $tempy, 40,$Hwnd) == 0 Then
			_Log("Skipping field X: " & $x & " Y: " & $y)
			ContinueLoop
		EndIf

		;Check level
		Local $resourceReadCropingSize = 3
		Local $x1 = $x - $resourceReadCropingSize
		Local $y1 = $y - $resourceReadCropingSize
		Local $x2 = $x + $resourceReadCropingSize
		Local $y2 = $y + $resourceReadCropingSize

		Local $level = GetLevel($x1,$y1,$x2,$y2)
		if $level < $lowestLevel Then
			$lowestLevel = $level
			$position[0] = $x
			$position[1] = $y

		EndIf
		_Log($level)
	Next

	Return $position
EndFunc

Func GetLevel($x1,$y1,$x2,$y2)

	Local $level = TesseractReadPoint($x1,$y1,$x2,$y2,1,10)

	;Weird behaviour with number 4. The scale needs to be to 2 for it to work...
	if Asc($level) == 46 Then;.
		$level = 4
	Elseif Asc($level) == 58 Then ; :
		$level = 8
	EndIf

	Return $level
EndFunc

Func ClickResourceView()
	Click($POSITION_RESOURCEVIEW[0],$POSITION_RESOURCEVIEW[1])
	Sleep(2000)
EndFunc