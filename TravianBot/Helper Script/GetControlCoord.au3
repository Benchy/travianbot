Opt("MouseCoordMode", 2)

$pos = mouseGetPos()
$x = $pos[0]
$y = $pos[1]

while(1)
HotKeySet("{#}","GetCoord")
WEnd


Func _WinWaitActivate($title,$text,$timeout=0)
	WinWait($title,$text,$timeout)
	If Not WinActive($title,$text) Then WinActivate($title,$text)
	WinWaitActive($title,$text,$timeout)
EndFunc

Func GetCoord()
	$pos = mouseGetPos()
	$x = $pos[0]
	$y = $pos[1]
	ClipPut($pos[0] & "," & $pos[1])

EndFunc