Func Click($x, $y, $times = 1, $speed = 0)
	If $times <> 1 Then
		For $i = 0 To ($times - 1)
			ControlClick($Hwnd, "", "", "left", "1", $x, $y)
			If Sleep($speed) Then ExitLoop
		Next
	Else
		ControlClick($Hwnd, "", "", "left", "1", $x, $y)
	EndIf
EndFunc   ;==>Click

Func ClickGlobal($x, $y, $times = 1, $speed = 0)
	$position = WinGetPos($Hwnd)
	$x = $x - $position[0]
	$y = $y - $position[1]

	If $times <> 1 Then
		For $i = 0 To ($times - 1)
			ControlClick($Hwnd, "", "", "left", "1", $x, $y)
			If Sleep($speed) Then ExitLoop
		Next
	Else
		ControlClick($Hwnd, "", "", "left", "1", $x, $y)
	EndIf
EndFunc

; ClickP : takes an array[2] (or array[4]) as a parameter [x,y]
Func ClickP($point, $howMuch = 1, $speed = 0)
	Click($point[0], $point[1], $howMuch, $speed)
EndFunc   ;==>ClickP

Func MouseMoveGlobal($x,$y)
	Local $pos = WinGetPos($Hwnd)
	MouseMove($x + $pos[0],$y + $pos[1])
EndFunc