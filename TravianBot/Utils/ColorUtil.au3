Func CompareColor($Color1, $Color2, $Variation)
    $C1Blue = _ColorGetBlue($Color1)
    $C1Green = _ColorGetGreen($Color1)
    $C1Red = _ColorGetRed($Color1)

    $C2Blue = _ColorGetBlue($Color2)
    $C2Green = _ColorGetGreen($Color2)
    $C2Red = _ColorGetRed($Color2)

    $BlueDiff = Abs($C1Blue - $C2Blue)
    $GreenDiff = Abs($C1Green - $C2Green)
    $RedDiff = Abs($C1Red - $C2Red)

    If ( ( $BlueDiff < $Variation ) AND ( $GreenDiff < $Variation ) AND ( $RedDiff < $Variation ) ) Then
        Return 1
    Else
        Return 0
    EndIf
EndFunc