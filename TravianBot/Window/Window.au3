#include <GuiEdit.au3>
Opt("WinTitleMatchMode",2)

Func _WinWaitActivate($title,$text,$timeout=0)
	if WinWait($title,$text,$timeout) == 0 Then
		Return 0
	EndIf

	If Not WinActive($title,$text) Then WinActivate($title,$text)
	If WinWaitActive($title,$text,$timeout) == 0 Then
		Return 0
	EndIf

	Return 1
EndFunc

Func Init()

	GUICtrlSetData($LogTxtBox,"")
	_TesseractTempPathSet("D:\")
	_GUICtrlListView_InsertColumn($ListAction, 0, "Actions", 300)
	InitVillages()


	Local $villageCount = _GUICtrlComboBox_GetCount($VillageCombo)

	For $i = 0 To $villageCount - 1 Step 1
		$SleepTime.Add(0)
	Next

	LocateWindow()
	FormatWindow()
EndFunc

Func LocateWindow()
	_Log("Locating window")
	Local $activated = 0
	While $activated == 0
		if _WinWaitActivate("Travian: Kingdoms","",0.01) == 1 Then
			$activated = 1
		Else
			if _WinWaitActivate("Report","",0.01) == 1 Then
				$activated = 1
			EndIf
		EndIf
	WEnd

	$Hwnd = WinGetHandle("")
	_Log("Window Located : " & $Hwnd)
EndFunc

Func FormatWindow()
	_Log("Formating Window")
	WinMove($Hwnd,"",Default,Default,$Resolution[0],$Resolution[1])
EndFunc

Func _Log($NewLogTxt)
	_GUICtrlEdit_AppendText($LogTxtBox,$NewLogTxt & @CRLF)
EndFunc

