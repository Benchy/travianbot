#Include <Color.au3>
#include <GuiListView.au3>

#include "Village\GlobalVillageInfo.au3"
#include "Village\ResourcesLeveler.au3"
#include "Village\BuildingLeveler.au3"
#include "Village\Adventure.au3"
#include "Village\Config.au3"

#include "Tesseract\Tesseract.au3"

#include "Utils\ArrayList.au3"
#include "Utils\ImageSearch.au3"
#include "Utils\Click.au3"
#include "Utils\ColorUtil.au3"


#include "Window\Window.au3"

#include "Globals.au3"
#include "ActionQueue.au3"

