Global $ActionQueue = ObjCreate('System.Collections.ArrayList')

Global $ACTION_AUTORESOURCE = "Auto Resource"
Global $ACTION_AUTORESOURCENOCROP = "Auto Resource no crop"
Global $ACTION_WOOD = "wood"
Global $ACTION_CLAY = "clay"
Global $ACTION_IRON = "iron"
Global $ACTION_CROP = "crop"

Global $ACTION_MAINBUILDING = "Main Building"
Global $ACTION_RALLYPOINT = "Rally Point"
Global $ACTION_TOURNEMANT = "Tournemant"
Global $ACTION_PALACE = "Palace"
Global $ACTION_MARKETPLACE = "Market Place"

Global $ACTION_BARRACK = "Barrack"
Global $ACTION_WAREHOUSE = "Warehouse"
Global $ACTION_GRANARY = "Granary"
Global $ACTION_TRADEOFFICE = "Trade office"

Global $ACTION_MILL = "Mill"
Global $ACTION_SAWMILL= "SawMill"
Global $ACTION_BRICKYARD = "Brick yard"
Global $ACTION_IRONFOUNDRY = "Iron foundry"

Global $ACTION_BAKERY = "Bakery"
Global $ACTION_ACADEMY = "Academy"
Global $ACTION_EMBASSY = "Embassy"
Global $ACTION_WORKSHOP = "Work shop"

Global $ACTION_STONEMASSON = "Stone Masson"
Global $ACTION_TOWNHALL = "Town Hall"
Global $ACTION_STABLE  = "Stable"
Global $ACTION_SMITHY= "Smithy"

Global $ACTION_PALISADE = "Palissade"

Global $ACTION_ADVENTURE = "Adventure"

Func AddAction($action)

	AddAction_2D($ActionQueue,$action)

	UpdateVisual()
EndFunc

Func ChangeVillage($nextIndex)

	_Log("Changing village to " & $nextIndex)

	while $nextIndex <> $CurrentVillageIndex

		Local $countVillage = _GUICtrlComboBox_GetCount($VillageCombo)

		if $nextIndex < $CurrentVillageIndex Then
			;click previous village
			Click(38,154)

			$CurrentVillageIndex = $CurrentVillageIndex - 1
			if $CurrentVillageIndex < 0 Then
				$CurrentVillageIndex = $countVillage - 1
			EndIf
		Else
			;click next village
			Click(199,154)

			$CurrentVillageIndex = $CurrentVillageIndex + 1
			if $CurrentVillageIndex > $countVillage-1 Then
				$CurrentVillageIndex = 0
			EndIf
		EndIf

		Sleep(500)
		_Log($CurrentVillageIndex)
		_GUICtrlComboBox_SetCurSel($VillageCombo,$CurrentVillageIndex)
		UpdateVisual()
	WEnd

	Sleep(2000)
EndFunc

Func PerformAction()
	if $ActionQueue.Count > 0 Then

		Local $currentAction = $ActionQueue.Item($CurrentVillageIndex).Item(0)

		$result = 0
		Switch $currentAction

		Case $ACTION_AUTORESOURCE
			$result = AutoLevelResources()
		Case $ACTION_AUTORESOURCENOCROP
			$result = AutoLevelResourcesNoCrop()
		Case $ACTION_WOOD
			$result = LevelResources($ACTION_WOOD)
		Case $ACTION_CLAY
			$result = LevelResources($ACTION_CLAY)
		Case $ACTION_IRON
			$result = LevelResources($ACTION_IRON)
		Case $ACTION_CROP
			$result = LevelResources($ACTION_CROP)

		Case $ACTION_MAINBUILDING
			$result = LevelBuilding($MainBuilding,$ACTION_MAINBUILDING)
		Case $ACTION_RALLYPOINT
			$result = LevelBuilding($RallyPoint,$ACTION_RALLYPOINT)
		Case $ACTION_TOURNEMANT
			$result = LevelBuilding($Tournemant,$ACTION_TOURNEMANT)
		Case $ACTION_PALACE
			$result = LevelBuilding($Palace,$ACTION_PALACE)
		Case $ACTION_MARKETPLACE
			$result = LevelBuilding($MarketPlace,$ACTION_MARKETPLACE)

		Case $ACTION_BARRACK
			$result = LevelBuilding($Barrack,$ACTION_BARRACK)
		Case $ACTION_WAREHOUSE
			$result = LevelBuilding($WareHouse,$ACTION_WAREHOUSE)
		Case $ACTION_GRANARY
			$result = LevelBuilding($Granary,$ACTION_GRANARY)
		Case $ACTION_TRADEOFFICE
			$result = LevelBuilding($TradeOffice,$ACTION_TRADEOFFICE)

		Case $ACTION_MILL
			$result = LevelBuilding($Mill,$ACTION_MILL)
		Case $ACTION_SAWMILL
			$result = LevelBuilding($SawMill,$ACTION_SAWMILL)
		Case $ACTION_BRICKYARD
			$result = LevelBuilding($BrickYard,$ACTION_BRICKYARD)
		Case $ACTION_IRONFOUNDRY
			$result = LevelBuilding($IronFoundry,$ACTION_IRONFOUNDRY)

		Case $ACTION_BAKERY
			$result = LevelBuilding($Bakery,$ACTION_BAKERY)
		Case $ACTION_ACADEMY
			$result = LevelBuilding($Academy,$ACTION_ACADEMY)
		Case $ACTION_EMBASSY
			$result = LevelBuilding($Embassy,$ACTION_EMBASSY)
		Case $ACTION_WORKSHOP
			$result = LevelBuilding($Workshop,$ACTION_WORKSHOP)

		Case $ACTION_STONEMASSON
			$result = LevelBuilding($StoneMasson,$ACTION_STONEMASSON)
		Case $ACTION_TOWNHALL
			$result = LevelBuilding($TownHall,$ACTION_TOWNHALL)
		Case $ACTION_STABLE
			$result = LevelBuilding($Stable,$ACTION_STABLE)
		Case $ACTION_SMITHY
			$result = LevelBuilding($Smithy,$ACTION_SMITHY)

		Case $ACTION_PALISADE
			$result = LevelBuilding($Palisade,$ACTION_PALISADE)

		Case $ACTION_ADVENTURE
			$result = StartAdventure()
		EndSwitch

		if $result == 1 Then
			if GUICtrlRead($loopCheckBox) <> $GUI_CHECKED Then
				$ActionQueue.Item($CurrentVillageIndex).RemoveAt(0)
			EndIf
			_Log("Action Succesful")
		Else
			Local $closex=0
			Local $closey=0
			_ImageSearchAreaHwnd($IMAGE_CLOSE,1, 0,0, $Resolution[0], $Resolution[1], $closex, $closey, 100,$Hwnd)

			ClickGlobal($closex,$closey)
			Sleep(2000)
			$SleepTime = 60
		EndIf

		UpdateVisual()
	EndIf
EndFunc

Func UpdateVisual()
	_GUICtrlListView_DeleteAllItems(GUICtrlGetHandle($ListAction))

	For $i = 0 to $ActionQueue.Item($CurrentVillageIndex).Count-1 Step 1
		_GUICtrlListView_AddItem($ListAction,$ActionQueue.Item($CurrentVillageIndex).Item($i),$i)
	Next
EndFunc